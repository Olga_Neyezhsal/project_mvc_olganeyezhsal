﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CarRentalSystem.Models;
using System.Diagnostics;
using System.IO;
using System.Data.SqlClient;

namespace CarRentalSystem.Controllers
{
    public class CarController : Controller
    {
         CarCMSContext db = new CarCMSContext();

        public ActionResult List()
        {
            Debug.WriteLine("I got to the list command");
            var cars = db.Cars.ToList();
            return View(cars.ToList());


        }
        // GET: Cars
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

       
        // GET: Cars/Details/5
    

        public ActionResult Details (int? id)
        {
            if ((id == null) || (db.Cars.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "select * from cars where carId = @id";
            SqlParameter param = new SqlParameter("@id", id);
            Car cartodetails = db.Cars.Find(id);

            return View(cartodetails);
            
        }
        


        // GET: Cars/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Cars/Create

        [HttpPost]
        public ActionResult Create(string car_title_new, string car_type_new, string car_description_new, string car_price_new, string car_orders_new)
        {

            //check to see if we have these values
            Debug.WriteLine(car_title_new + car_type_new + car_description_new + car_price_new + car_price_new + car_orders_new);


            string query = "insert into cars (title,type,description,price,orders) values (@title, @type,@description, @price,@orders)";
            SqlParameter[] myparams = new SqlParameter[5];
            myparams[0] = new SqlParameter("@title", car_title_new);
            myparams[1] = new SqlParameter("@type", car_type_new);
            myparams[2] = new SqlParameter("@description", car_description_new);
            myparams[3] = new SqlParameter("@price", car_price_new);
            myparams[4] = new SqlParameter("@orders", car_orders_new);

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");
        }



        // GET: Cars/Edit/5
        public ActionResult Edit(int? id)
        {
           
            if ((id == null) || (db.Cars.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "select * from cars where carId=@id";
            SqlParameter param = new SqlParameter("@id", id);
            Car mycar = db.Cars.SqlQuery(query, param).FirstOrDefault();
            return View(mycar);
        }
        // POST: Car/Edit/5

        [HttpPost]
        public ActionResult Edit (int id, string title, string type, string description, int price, string orders)
        {
            //I think 
            //Debug.WriteLine("I ARRIVED AT THE CAR EDIT METHOD");
            //return RedirectToAction("List");
            
            if ((id == null) || (db.Cars.Find(id)) == null)
            {
                return HttpNotFound();
            }

            string query = "update cars set title=@title, type=@type, description = @description, price = @price, orders = @orders where carId = @id";
            SqlParameter[] myparams = new SqlParameter[6];
            myparams[0] = new SqlParameter("@id", id);
            myparams[1] = new SqlParameter("@title", title);
            myparams[2] = new SqlParameter("@type", type);
            myparams[3] = new SqlParameter("@description", description);
            myparams[4] = new SqlParameter("@price", price);
            myparams[5] = new SqlParameter("@orders", orders);

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("Details/" + id);
            
        }

        // GET: Cars/Delete/5
      
       
             public ActionResult Delete(int? id)
        {
            if ((id == null) || (db.Cars.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "delete from cars where carId=@id";
            SqlParameter  param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, param);
            return RedirectToAction("List");

        }

    }
}
